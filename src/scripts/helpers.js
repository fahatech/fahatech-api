const nodeMailer = require("nodemailer");
const {
  random,
  isEmpty,
  filter,
  find,
  maxBy,
  minBy,
  get,
  includes,
} = require("lodash");
const moment = require("moment");

module.exports = {
  sendEmail: ({
    receiverName = ``,
    receiverEmail = ``,
    subject = ``,
    html = ``,
  }) => {
    const transporter = nodeMailer.createTransport({
      host: "smtp.gmail.com",
      port: 587,
      secure: false,
      auth: {
        user: "smtp.fahatech@gmail.com",
        pass: "Fahatech2022#",
      },
    });

    const options = {
      from: `${
        process.env.SENDER_NAME || "FAHATECH"
      } <smtp.fahatech@gmail.com>`,
      to: `${receiverName} <${receiverEmail}>`,
      subject, // Tiêu đề
      html,
    };
    return transporter.sendMail(options);
  },

  cleanUserData(data) {
    delete data.password;
    delete data.resetPasswordToken;
    delete data.confirmationToken;
    delete data.confirmed;
    delete data.verifyCode;
    delete data.resetPasswordCode;
    delete data.blocked;
    return data;
  },

  getQuantity(productObj = {}, taxIds = []) {
    // ĐÂY LÀ HÀM PHÂN TÍCH CÁC THUỘC TÍNH CỦA SẢN PHẨM VÀ LẤY RA SỐ LƯỢNG TỒN KHO
    let quantity = 0;
    if (productObj.priceList.length > 0) {
      // Có phân loại
      if (
        taxIds.length > 0 &&
        filter(taxIds).length === productObj.taxGroups.length
      ) {
        // Nếu đã chọn thuộc tính, và số thuộc tính đã chọn bằng số nhóm tuỳ chọn của sản phẩm

        const priceTaxObj = find(productObj.priceList, ["taxIds", taxIds]);
        if (!isEmpty(priceTaxObj)) {
          quantity = priceTaxObj.quantity;
        }
      } else {
        // Nếu có thuộc tính nhưng chưa chọn gì hết, hiển thị số lượng nhiều nhất

        let maxQuantity = maxBy(productObj.priceList, "quantity").quantity;
        quantity = +maxQuantity;
      }
    } else {
      quantity = +productObj.quantity;
    }
    return quantity;
  },

  getPrice(productObj = {}, taxIds = []) {
    // ĐÂY LÀ HÀM PHÂN TÍCH CÁC THUỘC TÍNH CỦA SẢN PHẨM VÀ LẤY RA GIÁ
    let price = 0;
    if (productObj.priceList.length > 0) {
      // Có phân loại
      if (
        taxIds.length > 0 &&
        filter(taxIds).length === productObj.taxGroups.length
      ) {
        // Nếu đã chọn thuộc tính, và số thuộc tính đã chọn bằng số nhóm tuỳ chọn của sản phẩm

        const priceTaxObj = find(productObj.priceList, ["taxIds", taxIds]);
        if (!isEmpty(priceTaxObj)) {
          price = priceTaxObj.price;
        }
      } else {
        // Nếu có thuộc tính nhưng chưa chọn gì hết, hiển thị khoảng giá

        let minPrice = minBy(productObj.priceList, "price").price;
        let maxPrice = maxBy(productObj.priceList, "price").price;
        if (minPrice === maxPrice) {
          // Nếu các giá bằng nhau hết thì hiển thị 1 cái
          price = maxPrice;
        } else {
          // ĐỐI VỚI TRANG CART VÀ CHECKOUT ĐIỀU KIỆN CHỖ NÀY KO THỂ XẢY RA,
          // VÌ NẾU KHÔNG CHỌN ĐỦ THUỘC TÍNH, GIÁ SẼ KO ĐƯỢC CHỌN => QUA TRANG CART VÀ CHECK SẼ KO CÓ
          price = [minPrice, maxPrice];
        }
      }
    } else {
      if (productObj.discountPrice) {
        // Nếu tồn tại giá khuyến mãi
        price = productObj.discountPrice;
      } else {
        price = productObj.price;
      }
    }
    // NẾU GIÁ ĐƠN THÌ TRẢ VỀ SỐ, KHOẢNG GIÁ THÌ TRẢ VỀ MẢNG
    return price;
  },

  getTotalProductsPrice(cartItems = []) {
    let totalPrice = 0;
    cartItems.map((cartItem) => {
      const product = cartItem.product;
      const subTotalPrice =
        module.exports.getPrice(product, cartItem.taxIds) * +cartItem.quantity;
      totalPrice = totalPrice + subTotalPrice;
    });
    return +totalPrice;
  },

  getTotalPrice(cartItems = [], discount = 0, shipCost = 0) {
    let totalPrice = +module.exports.getTotalProductsPrice(cartItems);

    if (+discount > 0) {
      totalPrice = totalPrice - discount;
    }
    if (+shipCost > 0) {
      totalPrice = totalPrice + shipCost;
    }
    if (totalPrice < 0) {
      totalPrice = 0;
    }
    return +totalPrice;
  },

  getDiscountCodeStatus(discountCodeObj) {
    const { startTime, endTime, limitActivate, activatedCount, isBlocked } =
      discountCodeObj;
    if (isBlocked) {
      return {
        id: 5,
        name: "Đã tạm khoá",
        color: "red",
      };
    } else if (startTime && moment(startTime) > moment()) {
      return {
        id: 1,
        name: "Chưa được kích hoạt",
        color: "purple",
      };
    } else if (endTime && moment(endTime) <= moment()) {
      return {
        id: 3,
        name: "Đã hết hạn",
        color: "default",
      };
    } else if (limitActivate && +activatedCount >= +limitActivate) {
      return {
        id: 4,
        name: "Hết lượt sử dụng",
        color: "default",
      };
    } else {
      return {
        id: 2,
        name: "Đang hoạt động",
        color: "green",
      };
    }
  },

  formatPhone(phone) {
    return phone
      ? phone.replace(
          /^\s*([0-9]{4})\s*\-?([0-9]{3})\s*\-?([0-9]{3})$/,
          "$1 $2 $3"
        )
      : "";
  },

  getTime(time, type = 1) {
    let dayMonthYear = moment(time).format("DD/MM/YYYY");
    let hour = moment(time).format("hh");
    if (+moment(time).format("H") === 0) {
      hour = `00`;
    }
    let minute = moment(time).format("mm");
    let amPm =
      moment(time).format("A") === "AM" || moment(time).format("A") === "SA"
        ? "sáng"
        : `chiều`;
    if (+moment(time).format("H") > 18) {
      amPm = `tối`;
    }
    if (+moment(time).format("H") === 12) {
      amPm = `trưa`;
    }

    if (type === 1) {
      return `${hour}:${minute} ${amPm} ngày ${dayMonthYear}`;
    }
    if (type === 2) {
      return `${dayMonthYear} lúc ${hour}:${minute} ${amPm}`;
    }
  },

  getTaxNamesSelected(productObj = {}, taxIds = []) {
    let taxNameList = [];
    if (taxIds.length > 0) {
      // Số lượng phân loại đã chọn > 0
      taxNameList = productObj.taxGroups.map((taxGroup) => {
        return filter(
          taxGroup.taxList.map((tax) => {
            if (includes(taxIds, +tax.taxId)) {
              return tax.taxName;
            }
          })
        );
      });
    } else {
      taxNameList = [];
    }
    return taxNameList;
  },

  getProductThumbnail(productObj = {}, taxIds = []) {
    // ĐÂY LÀ HÀM PHÂN TÍCH CÁC THUỘC TÍNH CỦA SẢN PHẨM VÀ LẤY RA ẢNH ĐẠI DIỆN
    let thumbnail = get(productObj, "thumbnails[0].thumbnail", ``);
    if (productObj.priceList.length > 0) {
      // Có phân loại
      if (
        taxIds.length > 0 &&
        filter(taxIds).length === productObj.taxGroups.length
      ) {
        // Nếu đã chọn thuộc tính, và số thuộc tính đã chọn bằng số nhóm tuỳ chọn của sản phẩm

        const priceTaxObj = find(productObj.priceList, ["taxIds", taxIds]);
        if (!isEmpty(priceTaxObj) && priceTaxObj.thumbnail) {
          thumbnail = priceTaxObj.thumbnail;
        }
      } else {
        // Nếu có thuộc tính nhưng chưa chọn gì hết, hiển thị số lượng nhiều nhất

        thumbnail = get(productObj, "thumbnails[0].thumbnail", ``);
      }
    } else {
      thumbnail = get(productObj, "thumbnails[0].thumbnail", ``);
    }
    return thumbnail;
  },

  formatMoney(value, suffix = "₫") {
    return (
      (Math.round(+value * 100) / 100)
        .toString()
        .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ` ${suffix}`
    );
  },
};
