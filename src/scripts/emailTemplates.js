const { get, isEmpty } = require("lodash");
const {
  formatPhone,
  getTime,
  getTaxNamesSelected,
  getProductThumbnail,
  formatMoney,
  getPrice,
} = require("./helpers");

module.exports = {
  authEmail: ({
    emailTitle = `Xác nhận đăng ký tài khoản`,
    emailMessage = `Cảm ơn quý khách đã tin tưởng và sử dụng dịch vụ của chúng tôi. Vui lòng sử dụng mã bên dưới để
    xác thực tài khoản:`,
    code = ``,
    customerName = ``,
    phone = ``,
    website = ``,
    LOGO = ``,
  }) => {
    const html = `
    <div
    class="email-container"
    style="
        background: #e8e8e8;
        width: 100%;
        text-align: center;
        font-size: 14px;
        line-height: 130%;
        padding: 10px;
        color: #4a4a4a;
        box-sizing: border-box;
    "
>
<table style=" border-radius: 5px; width: 100%; max-width: 520px; display: inline-block;  background: #fff;">
<tbody style="display: block;">
    <tr style="display: block;">
        <td style="display: block;">

    <div
        class="email-content"
        style="
            padding: 10px 15px 15px 15px;
            box-sizing: border-box;
        "
    >
        <div class="email-header" style="clear: both; width: 100%; float: left">
            <div class="email-logo" style="text-align: left">
                <a href="${website}" target="_blank" style="display: block">
                    <img
                        style="width: 130px; height: auto; margin-top: 10px"
                        src="${LOGO}"
                    />
                </a>
            </div>
        </div>
                        <div  class="email-line"
                        style="
                            clear: both;
                            width: 100%;
                            float: left;
                            border-top: 1px solid #c0392b;
                            height: 1px;
                            margin: 10px 0;
                        "></div>
        <div class="email-body" style="clear: both; width: 100%; float: left; text-align: left">
                <h1 style="text-align: center; font-size: 18px; margin-bottom: 30px; margin-top: 20px;  font-weight: 600;">${emailTitle}</h1>
                <p>Xin chào ${customerName},</p>
                    <p>
                        ${emailMessage}
                    </p>

                        <div style="text-align: center; margin: 30px 0px 10px 0px">
                            <div
                                style="
                                    background: #fafafa;
                                    border: 1px dashed #ef3139;
                                    padding: 10px 20px 8px 20px;
                                    border-radius: 5px;
                                    word-break: break-all;
                                    display: inline-block;
                                    margin: 0 auto;
                                    letter-spacing: 2px;
                                "
                            >
                                <span style="font-size: 18px; color: #ef3139; font-weight: 500;">${code}</span>
                            </div>
                        </div>


                            <div
                                class="email-line"
                                style="
                                    clear: both;
                                    width: 100%;
                                    float: left;
                                    border-top: 1px solid #f1f1f1;
                                    height: 1px;
                                    margin: 20px 0;
                                "
                            ></div>
                            <p style="color: #757d8a; font-size: 13px">
                                Để đảm bảo an toàn bảo mật, bảo vệ quyền và lợi ích của chính mình, vui lòng không chia sẻ thông
                                tin email này cho người khác. <br /><br />Mọi thông tin chi tiết, vui lòng truy cập website
                                <a style="color: #0a7cff" href="${website}" target="_blank">${website}</a>
                                hoặc liên hệ Hotline hỗ trợ khách hàng: ${phone} <br /><br />Xin chân thành cảm ơn quý
                                khách!
                            </p>
            </div>
                </td>
                    </tr>
                </tbody>
            </table>
        </div>
    `;

    return html;
  },

  orderEmail: ({
    sendTo = `customer`,
    emailTitle = ``,
    phone = ``,
    email = ``,
    website = ``,
    orderDetails = {},
    LOGO = ``,
  }) => {
    const {
      address = ``,
      province = {},
      district = {},
      ward = {},
    } = orderDetails.shippingAddress;
    const fullAddress = `${address || ``}, ${ward.type} ${ward.name}, 
    ${district.type} ${district.name}, 
    ${province.type} ${province.name}`;

    const cartItems = JSON.parse(orderDetails.cartItems || []);

    const html = `
    <div
            class="email-container"
            style="
                background: #e8e8e8;
                width: 100%;
                text-align: center;
                font-size: 14px;
                line-height: 130%;
                padding: 10px;
                color: #4a4a4a;
                box-sizing: border-box;
            "
        >
        <table style=" border-radius: 5px; width: 100%; max-width: 680px; display: inline-block;  background: #fff;">
        <tbody style="display: block;">
            <tr style="display: block;">
                <td style="display: block;">

            <div
                class="email-content"
                style="
                    padding: 10px 15px 15px 15px;
                    box-sizing: border-box;
                "
            >
                <div class="email-header" style="clear: both; width: 100%; float: left">
                    <div class="email-logo" style="text-align: left">
                        <a href="${website}" target="_blank" style="display: block">
                            <img
                                style="width: 130px; height: auto; margin-top: 10px"
                                src="${LOGO}"
                            />
                        </a>
                    </div>
                </div>
                <div
                    class="email-line"
                    style="
                        clear: both;
                        width: 100%;
                        float: left;
                        border-top: 1px solid #c0392b;
                        height: 1px;
                        margin: 10px 0;
                    "
                ></div>
                <div class="email-body" style="clear: both; width: 100%; float: left; text-align: left">
                    <h1
                        style="
                            text-align: center;
                            font-size: 16px;
                            margin-bottom: 15px;
                            margin-top: 10px;
                            font-weight: 600;
                            text-transform: uppercase;
                        "
                    >
                        ${emailTitle}
                    </h1>
                    
                    ${
                      sendTo === "customer"
                        ? `
                        <p>Xin chào ${get(orderDetails, 'owner.firstName', 'khách hàng')},</p>
                        <p>
                        Cảm ơn quý khách đã đặt hàng tại
                        <a style="color: #0a7cff; text-decoration: none; " href="${website}" target="_blank"
                            >${website.replace(
                              "https://",
                              ""
                            )}</a>. <br />Xin thông báo đơn hàng
                        <a style="color: #ef3139" href="${`${website}/account/orders/${orderDetails.orderNumber}`}" target="_blank">#${
                            orderDetails.orderNumber
                          }</a> của quý
                        khách đang trong quá trình xử lý.
                    </p>`
                        : `
                    <p>
                    Xin thông báo, có đơn hàng mới <a style="color: #ef3139" href="${`${process.env.ADMIN_URL}/orders/${orderDetails.id}`}" target="_blank">#${
                            orderDetails.orderNumber
                          }</a> từ website <a style="color: #0a7cff; text-decoration: none;  " href="${website}" target="_blank"
                      >${website.replace("https://", "")}</a>
                    </p>
                    `
                    }

                    <div style="margin-top: 20px; text-transform: uppercase;font-size: 13px;">
                        <label style="color: #555; font-weight: 600">Thông tin nhận hàng</label>
                    </div>
                    <div
                        style="
                            line-height: 140%;
                            color: #757d8a
                        "
                    >
                        <span style="font-weight: 600">${
                          orderDetails.customerName
                        }</span>
                        <br />
                        <span style="color: #007bff">${formatPhone(
                          orderDetails.phone
                        )}</span>
                        <br />
                        <span>${fullAddress}</span>
                    </div>

                    <div style="margin-top: 20px;  text-transform: uppercase;font-size: 13px;">
                        <label style="color: #555; font-weight: 600">Phương thức thanh toán</label>
                    </div>
                    <div
                        style="
                            line-height: 140%;
                            color: #757d8a
                        "
                    >
                        <span style="line-height: 120%">${
                          orderDetails.paymentMethod.name
                        }</span>
                    </div>

                    ${
                      orderDetails.note
                        ? `<div style="margin-top: 20px;  text-transform: uppercase;font-size: 13px;">
                        <label style="color: #555; font-weight: 600">Lời nhắn từ khách hàng</label>
                    </div>
                    <div
                        style="
                            line-height: 140%;
                            color: #757d8a
                        "
                    >
                        <span style="line-height: 120%">${orderDetails.note}</span>
                    </div>`
                        : ``
                    }

                    <div style="margin-top: 20px;  text-transform: uppercase;font-size: 13px;">
                        <label style="color: #555; font-weight: 600">Chi tiết đơn hàng</label>
                        <span style="float: right; color: #757d8a; font-size: 12px; text-transform: lowercase;">${getTime(
                          orderDetails.createdAt,
                          2
                        )}</span>
                    </div>

                    <div style="margin-top: 5px; vertical-align: top">
                        <table style="width: 100%; border: 1px solid #f1f1f1; border-collapse: collapse">
                            <thead>
                                <tr>
                                   
                                    <th style="padding: 8px 5px; background: #f1f1f1; font-weight: 500; min-width: 200px">Sản phẩm</th>
                                    <th style="padding: 8px 5px; background: #f1f1f1; font-weight: 500">Đơn giá</th>
                                    <th
                                        style="
                                            padding: 8px 5px;
                                            background: #f1f1f1;
                                            font-weight: 500;
                                            width: 65px;
                                            text-align: center;
                                        "
                                    >
                                        Số lượng
                                    </th>
                                    <th
                                        style="
                                            padding: 8px 5px;
                                            background: #f1f1f1;
                                            font-weight: 500;
                                            text-align: center;
                                        "
                                    >
                                        Thành tiền
                                    </th>
                                </tr>
                            </thead>
                            <tbody>

                            ${cartItems
                              .map((cartItem, index) => {
                                const product = cartItem.product;

                                const subTotalPrice =
                                  getPrice(product, cartItem.taxIds) *
                                  +cartItem.quantity;

                                return `<tr>
                                  
                                  <td style="vertical-align: top; padding: 5px; border-bottom: 1px solid #f1f1f1">
                                 
                                    <img
                                    src="${`${
                                      process.env.API_URL
                                    }/${getProductThumbnail(
                                      product,
                                      cartItem.taxIds
                                    )}`}"
                                      style="
                                                width: 65px;
                                                object-fit: contain;
                                                height: 65px;
                                                border: 1px solid #f1f1f1;
                                                border-radius: 2px;
                                                background: #fff;
                                                display: inline-block;
                                                vertical-align: top;
                                                margin-right: 5px;
                                            "
                                    />

                                    <span style="display: inline-block; width: calc(100% - 85px)">
                                      <a target="_blank" style="color: #4a4a4a; font-weight: 500; text-decoration: none;" href="${`${website}/products/${product.slug}`}">
                                       ${product.name}
                                      </a>
                                      <span style="display: block; margin-top: 3px">
                                        ${getTaxNamesSelected(
                                          product,
                                          cartItem.taxIds
                                        )
                                          .map((taxName) => {
                                            return `<small style="border: 1px solid #ccc;
                                                            padding: 0 4px;
                                                            margin: 0 2px;
                                                            color: #888;
                                                            border-radius: 2px;
                                                            font-size: 12px;">
                                            ${taxName}
                                            </small>`;
                                          })
                                          .join("")}
                                      </span>
                                    </span>
                                  </td>
                                  <td style="border-bottom: 1px solid #f1f1f1">
                                    <span style="color: #c0392b; padding: 5px; text-align: right; white-space: nowrap;">
                                    ${formatMoney(
                                      getPrice(product, cartItem.taxIds)
                                    )}
                                    </span>
                                  </td>
                                  <td style="text-align: center; padding: 5px; border-bottom: 1px solid #f1f1f1">
                                  ${cartItem.quantity}
                                  </td>
                                  <td style="border-bottom: 1px solid #f1f1f1; text-align: right">
                                    <strong style="color: #c0392b; padding: 5px; white-space: nowrap;">
                                      ${formatMoney(subTotalPrice)}
                                    </strong>
                                  </td>
                                </tr>`;
                              })
                              .join("")}


                                
                                <tr>
                                    <td
                                        colspan="3"
                                        style="padding: 8px 10px; text-align: right; border-right: 1px solid #f1f1f1"
                                    >
                                        Tổng tiền sản phẩm
                                    </td>
                                    <td colspan="2" style="padding: 8px 5px; text-align: right">
                                        <strong style="color: #c0392b; white-space: nowrap;">${formatMoney(
                                          orderDetails.itemsSubTotal
                                        )}</strong>
                                    </td>
                                </tr>
                                ${
                                  !isEmpty(orderDetails.discount)
                                    ? `
                                    <tr>
                                    <td
                                        colspan="3"
                                        style="padding: 8px 10px; text-align: right; border-right: 1px solid #f1f1f1; background: #fff2f0"
                                    >
                                      <span style="color: #531dab;
                                      background: #f9f0ff;
                                      border: 1px solid #d3adf7;
                                      display: inline-block;
                                      height: auto;
                                      margin-right: 8px;
                                      padding: 0 7px;
                                      font-size: 12px;
                                      line-height: 20px;
                                      ">${get(
                                        orderDetails,
                                        "discount.code"
                                      )}</span> Giảm giá
                                    </td>
                                    <td colspan="2" style="padding: 8px 5px; text-align: right; background: #fff2f0; white-space: nowrap;">- ${formatMoney(
                                      get(orderDetails, "discount.value", 0)
                                    )}</td>
                                </tr>
                                    `
                                    : ``
                                }
                               
                                <tr>
                                    <td
                                        colspan="3"
                                        style="padding: 8px 10px; text-align: right; border-right: 1px solid #f1f1f1"
                                    >
                                        Phí vận chuyển
                                    </td>
                                    <td colspan="2" style="padding: 8px 5px; text-align: right; white-space: nowrap;">${formatMoney(
                                      orderDetails.shipCost
                                    )}</td>
                                </tr>
                                <tr>
                                    <td
                                        colspan="3"
                                        style="
                                            padding: 8px 10px;
                                            text-align: right;
                                            border-bottom: 1px solid #f1f1f1;
                                            border-right: 1px solid #f1f1f1;
                                        "
                                    >
                                        <strong>Tổng giá trị đơn hàng</strong>
                                    </td>
                                    <td
                                        colspan="2"
                                        style="padding: 8px 5px; text-align: right; border-bottom: 1px solid #f1f1f1"
                                    >
                                        <strong style="color: #c0392b; font-size: 18px; white-space: nowrap;">${formatMoney(
                                          orderDetails.orderTotal
                                        )}</strong>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div style="text-align: center; margin-top: 20px">
                        <a
                            href="${
                              sendTo === "customer"
                                ? `${website}/account/orders/${orderDetails.orderNumber}`
                                : `${process.env.ADMIN_URL}/orders/${orderDetails.id}`
                            }"
                            target="_blank"
                            style="
                                height: 40px;
                                padding: 8px 30px;
                                border-radius: 2px;
                                background: #ef3139;
                                color: #fff;
                                text-decoration: none;
                            "
                            >Xem chi tiết đơn hàng
                        </a>
                    </div>

                   

                    ${
                      sendTo === "customer"
                        ? `
                        <div
                        class="email-line"
                        style="
                            clear: both;
                            width: 100%;
                            float: left;
                            border-top: 1px solid #f1f1f1;
                            height: 1px;
                            margin: 20px 0;
                        "
                    ></div>
                        
                        <p style="color: #757d8a; font-size: 13px">
                        Quý khách cần hỗ trợ, chỉ cần email đến <a style="color: #0a7cff" href="mailto:${email}" target="_blank">${email}</a>
                        hoặc liên hệ HOTLINE: 
                        <a style="color: #0a7cff; text-decoration: none;" href="tel:${phone}">${formatPhone(
                            phone
                          )}</a>.
                        <br />
                        Xin chân thành cảm ơn và rất mong tiếp tục nhận được sự ủng hộ của quý khách!
                    </p>`
                        : `<p></p>`
                    }
                </div>
            </div>
            </td>
                    </tr>
                </tbody>
            </table>
        </div>
    `;

    return html;
  },
};
