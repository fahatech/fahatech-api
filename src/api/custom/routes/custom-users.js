module.exports = {
  routes: [
    {
      method: "GET",
      path: "/v2/users",
      handler: "custom-users.getUserList",
      config: {
        policy: [],
      },
    },
    {
      method: "GET",
      path: "/v2/users/:userId",
      handler: "custom-users.getUserDetails",
      config: {
        policy: [],
      },
    },
    {
      method: "POST",
      path: "/v2/users",
      handler: "custom-users.createUser",
      config: {
        policy: [],
      },
    },
    {
      method: "PUT",
      path: "/v2/users/:userId",
      handler: "custom-users.updateUser",
      config: {
        policy: [],
      },
    },
  ],
};
