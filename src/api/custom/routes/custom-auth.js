module.exports = {
  routes: [
    {
      method: "POST",
      path: "/v2/login",
      handler: "custom-auth.login",
      config: {
        policy: [],
      },
    },
    {
      method: "POST",
      path: "/v2/login/facebook",
      handler: "custom-auth.loginFacebook",
      config: {
        policy: [],
      },
    },
    {
      method: "POST",
      path: "/v2/login/google",
      handler: "custom-auth.loginGoogle",
      config: {
        policy: [],
      },
    },
    {
      method: "POST",
      path: "/v2/register",
      handler: "custom-auth.register",
      config: {
        policy: [],
      },
    },
    {
      method: "POST",
      path: "/v2/verify-account",
      handler: "custom-auth.verifyAccount",
      config: {
        policy: [],
      },
    },
    {
      method: "POST",
      path: "/v2/resend-verify-code",
      handler: "custom-auth.resendVerifyCode",
      config: {
        policy: [],
      },
    },
    {
      method: "POST",
      path: "/v2/forgot-password",
      handler: "custom-auth.forgotPassword",
      config: {
        policy: [],
      },
    },
    {
      method: "PUT",
      path: "/v2/reset-password",
      handler: "custom-auth.resetPassword",
      config: {
        policy: [],
      },
    },
    {
      method: "GET",
      path: "/v2/profile",
      handler: "custom-auth.profile",
      config: {
        policy: [],
      },
    },
    {
      method: "PUT",
      path: "/v2/profile",
      handler: "custom-auth.updateProfile",
      config: {
        policy: [],
      },
    },
    {
      method: "PUT",
      path: "/v2/update-email",
      handler: "custom-auth.updateEmail",
      config: {
        policy: [],
      },
    },
    {
      method: "PUT",
      path: "/v2/change-password",
      handler: "custom-auth.changePassword",
      config: {
        policy: [],
      },
    },
  ],
};
