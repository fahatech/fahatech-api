module.exports = {
  routes: [
    {
      method: "POST",
      path: "/v2/orders",
      handler: "custom-orders.createOrder",
      config: {
        policy: [],
      },
    },
  ],
};
