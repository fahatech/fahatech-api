"use strict";

/**
 *  custom controller
 */
const moment = require("moment");
const {
  get,
  random,
  isEmpty,
  filter,
  find,
  maxBy,
  findIndex,
} = require("lodash");
const { createCoreController } = require("@strapi/strapi").factories;
const {
  cleanUserData,
  getQuantity,
  getTotalProductsPrice,
  getTotalPrice,
  getDiscountCodeStatus,
  sendEmail,
} = require("../../../scripts/helpers");

const { orderEmail } = require("../../../scripts/emailTemplates");

module.exports = createCoreController("api::custom.custom", ({ strapi }) => ({
  async createOrder(ctx) {
    // ======================================================================================================================
    const userService = strapi.service("plugin::users-permissions.user");
    const orderService = strapi.service("api::order.order");
    const productService = strapi.service("api::product.product");
    const notificationService = strapi.service(
      "api::notification.notification"
    );
    const cartService = strapi.service("api::cart-item.cart-item");
    const discountService = strapi.service("api::discount-code.discount-code");
    const appConfigData = await strapi
      .service("api::app-config.app-config")
      .find();
    // =================================================================

    const currentUser = ctx.state.user;
    if (!currentUser) {
      return ctx.unauthorized();
    }

    let reqData = ctx.request.body;
    const shipCost = +appConfigData.shipCost || 0;
    // ===============================

    // LẤY CART ITEMS HIỆN TẠI CỦA KHÁCH HÀNG ĐÓ, XỬ LÝ DATA CHO GỌN
    let cartItems = [];

    const cartItemsData = await cartService.find({
      filters: { owner: { id: { $eq: currentUser.id } } },
      populate: "*",
    });

    if (get(cartItemsData, "results", []).length === 0) {
      return ctx.badRequest(
        "Không có sản phẩm trong giỏ hàng, không thể đặt hàng.",
        {
          code: 3001,
        }
      );
    }

    cartItems = get(cartItemsData, "results", []).map((cartItem) => {
      return {
        ...cartItem,
        owner: cleanUserData(cartItem.owner),
      };
    });

    // ===============================

    const history = [
      {
        id: 1,
        time: moment(),
        orderStatus: {
          id: 1,
          name: `Chờ xác nhận`,
        },
        updatedBy: {
          id: currentUser.id,
          firstName: currentUser.firstName,
          lastName: currentUser.lastName,
        },
      },
    ];

    // ===============================

    const itemsSubTotal = +getTotalProductsPrice(cartItems);

    // KIỂM TRA HIỆU LỰC CỦA MÃ KHUYẾN MÃI VÀ ÁP DỤNG, SAU ĐÓ CẬP NHẬT LƯỢT SỬ DỤNG =================================
    let discountObj = {};
    if (reqData.discountId) {
      let currentDiscount = await discountService.findOne(reqData.discountId, {
        populate: "*",
      });

      if (!isEmpty(currentDiscount)) {
        const isValidDiscountCode = getDiscountCodeStatus(currentDiscount);

        if (+isValidDiscountCode.id === 2) {
          // ID bằng 2 tức là mã này đang hoạt động

          if (+currentDiscount.discountType === 1) {
            // GIẢM GIÁ THEO SỐ TIỀN
            discountObj = {
              id: currentDiscount.id,
              code: currentDiscount.discountCode,
              value: +currentDiscount.discountValue,
            };
          } else if (+currentDiscount.discountType === 2) {
            // GIẢM GIÁ THEO PHẦN TRĂM THÌ TÍNH RA SỐ TIỀN

            discountObj = {
              id: currentDiscount.id,
              code: currentDiscount.discountCode,
              value: +(itemsSubTotal * (+currentDiscount.discountValue / 100)),
            };
          }

          await discountService.update(currentDiscount.id, {
            data: {
              activatedCount: +(currentDiscount.activatedCount || 0) + 1,
            },
          });
        }
      }
    }

    const orderTotal = +getTotalPrice(cartItems, discountObj.value, shipCost);

    const orderNumber = random(11111111, 99999999).toString();

    reqData = {
      ...reqData,
      shipCost,
      orderNumber,
      history,
      owner: currentUser.id,
      createdByUser: currentUser.id,
      orderStatus: 1,
      itemsSubTotal,
      orderTotal,
      discount: discountObj,
      cartItems: JSON.stringify(cartItems),
    };

    // TẠO ORDER =================================
    const orderCreated = await orderService.create({
      data: reqData,
      populate: "*",
    });

    // CẬP NHẬT SỐ LƯỢNG TỒN KHO =================================
    const handleUpdateQuantity = async (cartItems = [], time = 0) => {
      const attributes = await cartItems[time];

      if (!isEmpty(attributes)) {
        const productId = attributes.product.id;
        let productObj = await productService.findOne(productId, {
          populate: "*",
        });

        if (!isEmpty(productObj)) {
          const productQuantity = +getQuantity(
            productObj,
            attributes.taxIds || []
          );

          const cartItemQuantity = +attributes.quantity;
          const newQuantity = productQuantity - cartItemQuantity;
          const newSoldNumber = (+productObj.sold || 0) + cartItemQuantity;

          if (attributes.taxIds.length > 0 && productObj.taxGroups.length > 0) {
            // NẾU ITEM NÀY TRONG GIỎ HÀNG LÀ SẢN PHẨM CÓ PHÂN LOẠI
            let priceTaxObj = find(productObj.priceList, [
              "taxIds",
              attributes.taxIds,
            ]);

            // TÌM PHÂN LOẠI CÓ ID KHỚP VỚI ID ĐÃ CHỌN, CẬP NHẬT SỐ LƯỢNG
            if (!isEmpty(priceTaxObj)) {
              priceTaxObj = {
                ...priceTaxObj,
                quantity: newQuantity > 0 ? newQuantity : 0,
              };

              let priceList = productObj.priceList;
              priceList[findIndex(priceList, ["taxId", priceTaxObj.taxId])] =
                priceTaxObj;

              await productService.update(productId, {
                data: {
                  priceList,
                  sold: +newSoldNumber,
                },
              });
            }
          } else {
            // CẬP NHẬT SỐ LƯỢNG
            await productService.update(productId, {
              data: {
                quantity: newQuantity > 0 ? newQuantity : 0,
                sold: +newSoldNumber,
              },
            });
          }
        }
        await new Promise((r) => setTimeout(r, 100));
        if (time !== cartItems.length) {
          // TIẾP TỤC VỚI ĐỆ QUY
          await handleUpdateQuantity(cartItems, time + 1);
        }
      }
    };
    await handleUpdateQuantity(cartItems);

    // XOÁ ITEM TRONG GIỎ HÀNG =================================
    await Promise.all(
      cartItems.map((cartItem) => {
        return cartService.delete(cartItem.id);
      })
    );

    // RESET TRẠNG THÁI ĐẶT HÀNG VÀO USER =================================
    await userService.edit(currentUser.id, {
      checkoutState: {},
    });

    // TẠO NOTIFICATION =================================
    await notificationService.create({
      data: {
        owner: currentUser.id,
        action: "createOrder",
        details: {
          orderId: orderCreated.id,
        },
      },
    });

    // console.log(`orderCreated =================================`, orderCreated);

    // GỬI MAIL CHO KHÁCH HÀNG =================================
    sendEmail({
      subject: `Đơn hàng #${orderCreated.orderNumber}`,
      html: orderEmail({
        sendTo: `customer`,
        emailTitle: `Thông tin đơn hàng #${orderCreated.orderNumber}`,
        phone: appConfigData.phone01 || appConfigData.phone02,
        email: appConfigData.email01 || appConfigData.email02,
        website: process.env.WEBSITE_URL,
        LOGO: `${process.env.API_URL}${appConfigData.footerLogo}`,
        orderDetails: orderCreated,
      }),
      receiverName: `${get(orderCreated, "owner.firstName", "")} ${get(
        orderCreated,
        "owner.lastName",
        ""
      )}`,
      receiverEmail: get(orderCreated, "owner.email", ""),
    });

    // GỬI MAIL CHO QUẢN TRỊ VIÊN =================================
    sendEmail({
      subject: `Đơn hàng mới #${orderCreated.orderNumber}`,
      html: orderEmail({
        sendTo: `admin`,
        emailTitle: `Thông tin đơn hàng #${orderCreated.orderNumber}`,
        phone: appConfigData.phone01 || appConfigData.phone02,
        email: appConfigData.email01 || appConfigData.email02,
        website: process.env.WEBSITE_URL,
        LOGO: `${process.env.API_URL}${appConfigData.footerLogo}`,
        orderDetails: orderCreated,
      }),
      receiverName: appConfigData.companyName || `FAHATECH`,
      receiverEmail: appConfigData.email01,
    });

    return {
      message: `Đặt hàng thành công`,
      data: {
        order: orderCreated,
      },
    };
  },
}));
