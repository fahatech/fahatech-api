"use strict";

/**
 *  custom controller
 */
const { random, isEmpty } = require("lodash");
const { createCoreController } = require("@strapi/strapi").factories;
const { cleanUserData } = require("../../../scripts/helpers");

module.exports = createCoreController("api::custom.custom", ({ strapi }) => ({
  async getUserList(ctx) {
    // ======================================================================================================================
    const userService = strapi.service("plugin::users-permissions.user");

    if (!ctx.state.user) {
      return ctx.unauthorized();
    }

    const { page, pageSize } = ctx.query.pagination;

    let userListData = await strapi
      .query("plugin::users-permissions.user")
      .findMany({
        where: ctx.query.filters,
        populate: ["role"],
        limit: pageSize,
        offset: pageSize * page - pageSize,
      });

    userListData = userListData.map((user) => ({
      id: user.id,
      attributes: cleanUserData(user),
    }));

    let totalUsers = await userService.count(ctx.query.filters);

    return {
      data: {
        users: userListData,
        totalUsers: totalUsers || 0,
      },
    };
  },

  async getUserDetails(ctx) {
    // ======================================================================================================================
    const userService = strapi.service("plugin::users-permissions.user");

    if (!ctx.state.user) {
      return ctx.unauthorized();
    }

    const { userId } = ctx.request.params;

    const userData = await userService.fetch({ id: userId }, ["role"]);
    return {
      data: {
        user: cleanUserData(userData),
      },
    };
  },

  async createUser(ctx) {
    // ======================================================================================================================
    const userService = strapi.service("plugin::users-permissions.user");
    const currentUser = ctx.state.user;
    if (!currentUser) {
      return ctx.unauthorized();
    }

    let reqData = ctx.request.body;

    const { email, password, firstName, lastName } = ctx.request.body;
    if (!email || !password || !firstName || !lastName) {
      return ctx.badRequest(
        "Vui lòng cung cấp đủ thông tin để tạo tài khoản.",
        { code: 1001 }
      );
    }

    const userData = await userService.fetch(
      { email: reqData.email, provider: "local" },
      ["role"]
    );

    if (!isEmpty(userData)) {
      return ctx.badRequest(
        "Email này đã được sử dụng, vui lòng chọn một email khác.",
        { code: 1002 }
      );
    }

    // NẾU QUYỀN CỦA USER ĐANG ĐĂNG NHẬP NHỎ HƠN HOẶC BẰNG ROLE TRUYỀN VÀO, THÌ CHẶN
    // LƯU Ý, ROLE 1 LÀ LỚN NHẤT
    if (+currentUser.role.id >= +reqData.role) {
      return ctx.badRequest(
        "Bạn không có quyền tạo người dùng với vai trò này.",
        {
          code: 2003,
        }
      );
    }

    reqData = {
      ...reqData,
      username: `fahatech_${email}`,
      provider: "local",
      confirmed: true,
      isConfirmed: true,
      role: reqData.role ? reqData.role : 4,
      type: reqData.role && reqData.role !== 4 ? 1 : 0,
    };

    const userCreated = await userService.add(reqData);

    return {
      data: {
        user: cleanUserData(userCreated),
      },
    };
  },

  async updateUser(ctx) {
    // ======================================================================================================================
    const userService = strapi.service("plugin::users-permissions.user");
    const currentUser = ctx.state.user;
    if (!currentUser) {
      return ctx.unauthorized();
    }

    const { userId } = ctx.request.params;
    let reqData = ctx.request.body;

    const userData = await userService.fetch({ id: userId }, ["role"]);

    if (isEmpty(userData)) {
      return ctx.badRequest("Tài khoản này không tồn tại trên hệ thống.", {
        code: 1003,
      });
    }

    // NẾU QUYỀN CỦA USER ĐANG ĐĂNG NHẬP NHỎ HƠN HOẶC BẰNG QUYỀN USER MUỐN UPDATE, THÌ CHẶN
    // LƯU Ý, ROLE 1 LÀ LỚN NHẤT
    if (
      +currentUser.id !== +userData.id &&
      +currentUser.role.id >= +userData.role.id
    ) {
      return ctx.badRequest(
        "Bạn không có quyền chỉnh sửa thông tin người dùng này.",
        {
          code: 2001,
        }
      );
    }

    // NẾU QUYỀN CỦA USER ĐANG ĐĂNG NHẬP NHỎ HƠN HOẶC BẰNG ROLE TRUYỀN VÀO, THÌ CHẶN
    // LƯU Ý, ROLE 1 LÀ LỚN NHẤT
    if (+currentUser.role.id >= +reqData.role) {
      return ctx.badRequest(
        "Bạn không có quyền chỉnh sửa người dùng với vai trò này.",
        {
          code: 2003,
        }
      );
    }

    delete reqData.provider;
    delete reqData.type;
    delete reqData.username;

    if (reqData.email !== userData.email) {
      // NẾU REQUEST CÓ GỬI EMAIL, VÀ KHÁC VỚI EMAIL HIỆN TẠI CỦA TÀI KHOẢN, THÌ CHECK XEM CÓ TRÙNG KO?
      const isDuplicate = await userService.fetch(
        { email: reqData.email, provider: "local" },
        ["role"]
      );
      if (!isEmpty(isDuplicate)) {
        return ctx.badRequest(
          "Email này đã được sử dụng, vui lòng chọn một email khác.",
          { code: 1002 }
        );
      }
      reqData.username = `fahatech_${reqData.email}`;
    }

    if (+currentUser.id === +userData.id) {
      // NẾU ID MUỐN SỬA TRÙNG VỚI NGƯỜI ĐANG ĐĂNG NHẬP (TỨC LÀ GIỐNG UPDATE PROFILE)

      if (reqData.role !== +userData.role.id) {
        return ctx.badRequest("Không thể tự sửa đổi vai trò của mình.", {
          code: 2002,
        });
      }
      delete reqData.isConfirmed;
      // KHÔNG THỂ TỰ MÌNH XÁC THỰC, BẮT BUỘC PHẢI THÔNG QUA EMAIL
    }

    let updatedUserData = await userService.edit(userId, {
      ...reqData,
    });

    return {
      data: {
        user: cleanUserData(updatedUserData),
      },
    };
  },
}));
