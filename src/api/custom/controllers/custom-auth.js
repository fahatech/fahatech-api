"use strict";

/**
 *  custom controller
 */
const { get, random, isEmpty } = require("lodash");
const axios = require("axios");
const { createCoreController } = require("@strapi/strapi").factories;
const { sendEmail, cleanUserData } = require("../../../scripts/helpers");
const { authEmail } = require("../../../scripts/emailTemplates");

module.exports = createCoreController("api::custom.custom", ({ strapi }) => ({
  async register(ctx) {
    // ======================================================================================================================
    // Gõ lệnh này để xem controller: npm run strapi controllers:list
    const userService = strapi.service("plugin::users-permissions.user");
    const appConfigData = await strapi
      .service("api::app-config.app-config")
      .find();

    const { email, password, firstName, lastName } = ctx.request.body;
    if (!email || !password || !firstName || !lastName) {
      return ctx.badRequest(
        "Vui lòng cung cấp đủ thông tin để đăng ký tài khoản.",
        { code: 1001 }
      );
    }

    const userData = await userService.fetch({ email, provider: "local" }, [
      "role",
    ]);
    if (!isEmpty(userData)) {
      return ctx.badRequest(
        "Email này đã được sử dụng, vui lòng chọn một email khác.",
        { code: 1002 }
      );
    }

    const verifyCode = random(111111, 999999);

    const reqData = {
      email,
      username: `fahatech_${email}`,
      firstName,
      lastName,
      password,
      verifyCode,
      provider: "local",
      role: 4,
      confirmed: true,
    };

    const userCreated = await userService.add(reqData);

    if (!isEmpty(userCreated) && !isEmpty(appConfigData)) {
      sendEmail({
        subject: `Xác nhận đăng ký tài khoản tại ${
          appConfigData.companyName || "FAHATECH"
        }`,
        html: authEmail({
          code: verifyCode,
          customerName: userCreated.firstName,
          phone: appConfigData.phone01 || appConfigData.phone02,
          website: process.env.WEBSITE_URL,
          LOGO: `${process.env.API_URL}${appConfigData.footerLogo}`,
        }),
        receiverName: `${userCreated.firstName} ${userCreated.lastName}`,
        receiverEmail: email,
      });

      return {
        code: 1009,
        message:
          "Đăng ký thành công, chúng tôi đã gửi một mã xác thực tới Email của bạn, vui lòng kiểm tra hộp thư, nhập mã để kích hoạt tài khoản.",
        data: {
          id: userCreated.id,
          email: userCreated.email,
        },
      };
    }
  },

  async verifyAccount(ctx) {
    // ======================================================================================================================
    const { id, verifyCode } = ctx.request.body;

    const jwtService = strapi.service("plugin::users-permissions.jwt");
    const userService = strapi.service("plugin::users-permissions.user");
    const userData = await userService.fetch({ id }, ["role"]);

    if (isEmpty(userData)) {
      return ctx.badRequest("Tài khoản này không tồn tại trên hệ thống.", {
        code: 1003,
      });
    }

    if (userData.isConfirmed) {
      return ctx.badRequest("Tài khoản này đã được xác thực trước đó.", {
        code: 1011,
      });
    }

    if (+userData.verifyCode === +verifyCode) {
      let confirmedUserData = await userService.edit(id, {
        isConfirmed: true,
        verifyCode: null,
      });

      return {
        code: 1010,
        message: "Xác thực tài khoản thành công.",
        data: {
          jwt: jwtService.issue({
            id: confirmedUserData.id,
          }),
          user: cleanUserData(confirmedUserData),
        },
      };
    } else {
      return ctx.badRequest(
        "Mã xác thực không đúng hoặc đã hết hiệu lực, vui lòng kiểm tra lại.",
        {
          code: 1004,
        }
      );
    }
  },

  async resendVerifyCode(ctx) {
    // ======================================================================================================================
    const userService = strapi.service("plugin::users-permissions.user");
    const appConfigData = await strapi
      .service("api::app-config.app-config")
      .find();

    const { email } = ctx.request.body;
    let userToUpdate = {};
    let emailSubject = `Mã xác thực tài khoản tại`;
    let emailTitle = `Mã xác thực đăng ký tài khoản`;
    let emailMessage = `Chúng tôi đã nhận được yêu cầu gửi lại mã xác thực cho tài khoản của bạn. Vui lòng sử dụng mã bên dưới để
    xác thực tài khoản:`;

    if (!isEmpty(ctx.state.user)) {
      // NẾU USER ĐÃ ĐĂNG NHẬP
      userToUpdate = ctx.state.user;
      emailSubject = `Mã xác thực thay đổi Email`;
      emailTitle = `Mã xác thực thay đổi Email`;
      emailMessage = `Chúng tôi đã nhận được yêu cầu thay đổi Email cho tài khoản của bạn.
      Vui lòng sử dụng mã bên dưới để thay đổi Email:`;
    } else {
      // NẾU CHƯA ĐĂNG NHẬP THÌ DÙNG EMAIL TÌM USER RỒI MỚI GỬI
      const userData = await userService.fetch({ email, provider: "local" }, [
        "role",
      ]);
      if (isEmpty(userData)) {
        return ctx.badRequest("Tài khoản này không tồn tại trên hệ thống.", {
          code: 1003,
        });
      }
      userToUpdate = userData;
    }

    const verifyCode = random(111111, 999999);

    // CẬP NHẬT LẠI VERIFY CODE MỚI, SAU ĐÓ GỬI MAIL
    await userService.edit(userToUpdate.id, {
      verifyCode,
    });

    sendEmail({
      subject: `${emailSubject} ${appConfigData.companyName || "FAHATECH"}`,
      html: authEmail({
        code: verifyCode,
        emailTitle,
        emailMessage,
        customerName: userToUpdate.firstName,
        phone: appConfigData.phone01 || appConfigData.phone02,
        website: process.env.WEBSITE_URL,
        LOGO: `${process.env.API_URL}${appConfigData.footerLogo}`,
      }),
      receiverName: `${userToUpdate.firstName} ${userToUpdate.lastName}`,
      receiverEmail: email,
    });

    return {
      message: "Gửi mã xác thực thành công.",
      data: {
        id: userToUpdate.id,
        email,
      },
    };
  },

  async forgotPassword(ctx) {
    // ======================================================================================================================
    const userService = strapi.service("plugin::users-permissions.user");
    const appConfigData = await strapi
      .service("api::app-config.app-config")
      .find();

    const { email } = ctx.request.body;

    let userData = await userService.fetch({ email, provider: "local" }, [
      "role",
    ]);

    if (isEmpty(userData)) {
      return ctx.badRequest(
        "Email không tồn tại trong hệ thống, vui lòng kiểm tra lại.",
        {
          code: 1005,
        }
      );
    }

    const resetPasswordCode = random(111111, 999999);
    // CẬP NHẬT LẠI RESET CODE MỚI, SAU ĐÓ GỬI MAIL
    await userService.edit(userData.id, {
      resetPasswordCode,
    });

    sendEmail({
      subject: `Mã khôi phục mật khẩu tại ${
        appConfigData.companyName || "FAHATECH"
      }`,
      html: authEmail({
        emailTitle: `Mã khôi phục mật khẩu`,
        emailMessage: `Chúng tôi đã nhận được yêu cầu đặt lại mật khẩu của bạn.
        Vui lòng sử dụng mã bên dưới để đặt lại mật khẩu:`,
        code: resetPasswordCode,
        customerName: userData.firstName,
        phone: appConfigData.phone01 || appConfigData.phone02,
        website: process.env.WEBSITE_URL,
        LOGO: `${process.env.API_URL}${appConfigData.footerLogo}`,
      }),
      receiverName: `${userData.firstName} ${userData.lastName}`,
      receiverEmail: email,
    });

    return {
      message: "Gửi yêu cầu đặt lại mật khẩu thành công.",
      data: {
        id: userData.id,
        email,
      },
    };
  },

  async resetPassword(ctx) {
    const { id, resetPasswordCode, password } = ctx.request.body;

    const userService = strapi.service("plugin::users-permissions.user");
    let userData = await userService.fetch({ id }, ["role"]);
    if (isEmpty(userData)) {
      return ctx.badRequest("Tài khoản này không tồn tại trên hệ thống.", {
        code: 1003,
      });
    }

    if (+userData.resetPasswordCode !== +resetPasswordCode) {
      return ctx.badRequest(
        "Mã khôi phục mật khẩu không đúng hoặc đã hết hiệu lực, vui lòng kiểm tra lại.",
        {
          code: 1004,
        }
      );
    }
    userData = await userService.edit(id, {
      password,
      resetPasswordCode: null,
    });

    return {
      code: 1010,
      message: "Đặt lại mật khẩu thành công.",
      data: {
        user: cleanUserData(userData),
      },
    };
  },

  async login(ctx) {
    // ======================================================================================================================
    const { email, password } = ctx.request.body;

    const jwtService = strapi.service("plugin::users-permissions.jwt");
    const userService = strapi.service("plugin::users-permissions.user");
    const userData = await userService.fetch({ email, provider: "local" }, [
      "role",
    ]);

    if (isEmpty(userData)) {
      return ctx.badRequest(
        "Email không tồn tại trong hệ thống, vui lòng kiểm tra lại.",
        {
          code: 1005,
        }
      );
    }

    const isValidPassword = await userService.validatePassword(
      password,
      userData.password
    );

    if (!isValidPassword) {
      return ctx.badRequest("Sai mật khẩu, vui lòng kiểm tra lại.", {
        code: 1006,
      });
    }

    if (!userData.isConfirmed) {
      return ctx.badRequest(
        "Tài khoản này chưa được xác thực, vui lòng xác thực tài khoản để sử dụng dịch vụ.",
        {
          code: 1007,
          data: {
            id: userData.id,
          },
        }
      );
    }

    if (userData.isBlocked) {
      return ctx.badRequest(
        "Tài khoản này đã bị khoá, vui lòng liên hệ quản trị viên để mở khoá.",
        {
          code: 1008,
        }
      );
    }

    return {
      data: {
        jwt: jwtService.issue({
          id: userData.id,
        }),
        user: cleanUserData(userData),
      },
    };
  },

  async loginFacebook(ctx) {
    // ======================================================================================================================
    const jwtService = strapi.service("plugin::users-permissions.jwt");
    const userService = strapi.service("plugin::users-permissions.user");

    const { socialToken } = ctx.request.body;

    try {
      const { data: resFacebookData } = await axios({
        method: "get",
        url: `https://graph.facebook.com/me?fields=id,email,picture,first_name,last_name&access_token=${socialToken}`,
      });

      const socialUserId = resFacebookData.id;
      let userData = await userService.fetch(
        { username: `fahatech_${socialUserId}` },
        ["role"]
      );

      if (isEmpty(userData)) {
        const reqData = {
          email: resFacebookData.email
            ? resFacebookData.email
            : `facebook_${socialUserId}@fahatech.com`,
          username: `fahatech_${socialUserId}`,
          firstName: resFacebookData.first_name || ``,
          lastName: resFacebookData.last_name || ``,
          avatar: `https://graph.facebook.com/${socialUserId}/picture?width=300&height=300`,
          provider: "facebook",
          role: 4,
          confirmed: true,
          isConfirmed: true,
        };

        userData = await userService.add(reqData);
      } else {
        // NẾU NGƯỜI DÙNG CHƯA ĐỔI AVATAR THÌ UPDATE TỪ FACEBOOK, ĐỔI RỒI THÌ THÔI
        let dataToUpdate = {
          firstName: resFacebookData.first_name || ``,
          lastName: resFacebookData.last_name || ``,
          email: resFacebookData.email
            ? resFacebookData.email
            : `facebook_${socialUserId}@fahatech.com`,
        };
        if (userData.avatar.substring(0, 27) === "https://graph.facebook.com") {
          dataToUpdate.avatar = `https://graph.facebook.com/${socialUserId}/picture?width=300&height=300`;
        }
        userData = await userService.edit(userData.id, dataToUpdate);
      }

      return {
        data: {
          jwt: jwtService.issue({
            id: userData.id,
          }),
          user: cleanUserData(userData),
        },
      };
    } catch {
      return ctx.badRequest(
        "Đã có lỗi xảy ra, không thể đăng nhập bằng Facebook. Xin vui lòng thử đăng nhập với Google hoặc Email",
        {
          code: 1012,
        }
      );
    }
  },

  async loginGoogle(ctx) {
    // ======================================================================================================================
    const jwtService = strapi.service("plugin::users-permissions.jwt");
    const userService = strapi.service("plugin::users-permissions.user");

    const { socialToken } = ctx.request.body;

    try {
      const { data: resGoogleData } = await axios({
        method: "get",
        url: `https://www.googleapis.com/oauth2/v3/userinfo?access_token=${socialToken}`,
      });

      const socialUserId = resGoogleData.sub;
      let userData = await userService.fetch(
        { username: `fahatech_${socialUserId}` },
        ["role"]
      );

      if (isEmpty(userData)) {
        const reqData = {
          email: resGoogleData.email
            ? resGoogleData.email
            : `google_${socialUserId}@fahatech.com`,
          username: `fahatech_${socialUserId}`,
          firstName: resGoogleData.given_name || ``,
          lastName: resGoogleData.family_name || ``,
          avatar: resGoogleData.picture,
          provider: "google",
          role: 4,
          confirmed: true,
          isConfirmed: true,
        };

        userData = await userService.add(reqData);
      } else {
        userData = await userService.edit(userData.id, {
          firstName: resGoogleData.given_name || ``,
          lastName: resGoogleData.family_name || ``,
          email: resGoogleData.email
            ? resGoogleData.email
            : `google_${socialUserId}@fahatech.com`,
          username: `fahatech_${socialUserId}`,
        });
        userData = await userService.edit(userData.id, userData);
      }

      return {
        data: {
          jwt: jwtService.issue({
            id: userData.id,
          }),
          user: cleanUserData(userData),
        },
      };
    } catch {
      return ctx.badRequest(
        "Đã có lỗi xảy ra, không thể đăng nhập bằng Google. Xin vui lòng thử đăng nhập với Facebook hoặc Email",
        {
          code: 1013,
        }
      );
    }
  },

  async profile(ctx) {
    // ======================================================================================================================
    const currentUser = ctx.state.user;
    if (!currentUser) {
      return ctx.unauthorized();
    }
    return {
      data: {
        user: cleanUserData(currentUser),
      },
    };
  },

  async updateProfile(ctx) {
    // ======================================================================================================================
    const userService = strapi.service("plugin::users-permissions.user");

    const currentUser = ctx.state.user;
    if (!currentUser) {
      return ctx.unauthorized();
    }

    let reqData = ctx.request.body;
    delete reqData.role;
    delete reqData.provider;

    delete reqData.username;
    delete reqData.type;
    delete reqData.isBlocked;
    delete reqData.isConfirmed;
    delete reqData.password;
    delete reqData.email; // EMAIL FẢI CẬP NHẬT RIÊNG

    let updatedUserData = await userService.edit(currentUser.id, {
      // ...currentUser,
      ...reqData,
    });

    return {
      data: {
        user: cleanUserData(updatedUserData),
      },
    };
  },

  async updateEmail(ctx) {
    // ======================================================================================================================
    const userService = strapi.service("plugin::users-permissions.user");

    const currentUser = ctx.state.user;
    if (!currentUser) {
      return ctx.unauthorized();
    }

    const { email, verifyCode } = ctx.request.body; // ĐÂY LÀ EMAIL MỚI

    if (+verifyCode !== +currentUser.verifyCode) {
      return ctx.badRequest(
        "Mã xác thực không đúng hoặc đã hết hiệu lực, vui lòng kiểm tra lại.",
        {
          code: 1004,
        }
      );
    }

    const isDuplicate = await userService.fetch({ email, provider: "local" }, [
      "role",
    ]);
    if (!isEmpty(isDuplicate)) {
      return ctx.badRequest(
        "Email này đã được sử dụng, vui lòng chọn một email khác.",
        { code: 1002 }
      );
    }

    let updatedUserData = await userService.edit(currentUser.id, {
      email,
      username: `fahatech_${email}`,
      verifyCode: null,
    });

    return {
      data: {
        user: cleanUserData(updatedUserData),
      },
    };
  },

  async changePassword(ctx) {
    // ======================================================================================================================
    const userService = strapi.service("plugin::users-permissions.user");

    const currentUser = ctx.state.user;
    if (!currentUser) {
      return ctx.unauthorized();
    }

    const { oldPassword, newPassword } = ctx.request.body;

    const isValidOldPassword = await userService.validatePassword(
      oldPassword,
      currentUser.password
    );

    if (!isValidOldPassword) {
      return ctx.badRequest("Mật khẩu cũ không đúng, vui lòng kiểm tra lại.", {
        code: 1006,
      });
    }

    let updatedUserData = await userService.edit(currentUser.id, {
      password: newPassword,
    });

    return {
      message: "Đã thay đổi mật khẩu thành công.",
      data: {
        user: cleanUserData(updatedUserData),
      },
    };
  },
}));
