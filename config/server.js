const { random } = require("lodash");
module.exports = ({ env }) => ({
  host: env("HOST", "0.0.0.0"),
  port:
    process.env.NODE_ENV === "production"
      ? random(1111, 9999)
      : env.int("PORT", 1337),
  app: {
    keys: env.array("APP_KEYS"),
  },
});
