module.exports = ({ env }) => ({
  // email: {
  //   config: {
  //     provider: "strapi-provider-email-sendinblue",
  //     providerOptions: {
  //       sendinblue_api_key: env(
  //         "SENDINBLUE_KEY",
  //         "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"
  //       ),
  //       sendinblue_default_replyto: env("SENDER_EMAIL", "info@fahatech.com"),
  //       sendinblue_default_from: env("SENDER_EMAIL", "info@fahatech.com"),
  //       sendinblue_default_from_name: env("SENDER_NAME", "FAHATECH"),
  //     },
  //   },
  // },

  graphql: {
    config: {
      endpoint: "/graphql",
      shadowCRUD: true,
      playgroundAlways: false,
      depthLimit: 70,
      amountLimit: 1000,
      apolloServer: {
        tracing: false,
      },
    },
  },
  "users-permissions": {
    config: {
      jwt: {
        expiresIn: "30d",
      },
    },
  },

  // "@strapi/provider-upload-aws-s3": "^4.1.5",
  // upload: {
  //   config: {
  //     provider: "aws-s3",
  //     providerOptions: {
  //       accessKeyId: env("AKIAYHC5PHBAXRKLR6NO"),
  //       secretAccessKey: env("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"),
  //       region: env("ap-southeast-1"),
  //       params: {
  //         Bucket: env("fahatech"),
  //       },
  //     },
  //   },
  // },
});
