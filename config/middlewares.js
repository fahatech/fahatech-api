module.exports = [
  "strapi::errors",
  "strapi::security",
  "strapi::cors",
  // {
  //   name: "strapi::cors",
  //   config: {
  //     enabled: true,
  //     header: "*",
  //     origin: ["http://localhost:1337", "http://localhost:6888"],
  //   },
  // },
  // {
  //   name: "strapi::security",
  //   config: {
  //     contentSecurityPolicy: {
  //       useDefaults: true,
  //       directives: {
  //         "connect-src": ["'self'", "https:"],
  //         "img-src": [
  //           "'self'",
  //           "data:",
  //           "blob:",
  //           "fahatech.s3.ap-southeast-1.amazonaws.com",
  //         ],
  //         "media-src": [
  //           "'self'",
  //           "data:",
  //           "blob:",
  //           "fahatech.s3.ap-southeast-1.amazonaws.com",
  //         ],
  //         upgradeInsecureRequests: null,
  //       },
  //     },
  //   },
  // },
  "strapi::poweredBy",
  "strapi::logger",
  "strapi::query",
  "strapi::body",
  "strapi::session",
  "strapi::favicon",
  "strapi::public",
];
